<?php

/**
 * This file is part of the symfony.
 */

namespace App\FormSubscriber;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @package App\FormSubscriber
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class NewsTypeSubscriber implements EventSubscriberInterface
{
    
    public static function getSubscribedEvents(): array 
    {
        return [
            FormEvents::POST_SET_DATA => 'postSetData',
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }

    public function postSetData(FormEvent $event)
    {
        $event->getForm()
            ->add('title', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
                'attr' => ['onchange' => 'alert("aa")']
            ]);
    }
    
    public function postSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (strlen($data->getTitle()) > 4) {
            $event
                ->getForm()
                ->get('title')
                ->addError(new FormError("judul tidak boleh lebih dari 4 karakter"));
        }
    }
}
