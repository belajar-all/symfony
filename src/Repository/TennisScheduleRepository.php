<?php

namespace App\Repository;

use App\Entity\TennisSchedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TennisSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method TennisSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method TennisSchedule[]    findAll()
 * @method TennisSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TennisScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TennisSchedule::class);
    }

    // /**
    //  * @return TennisSchedule[] Returns an array of TennisSchedule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TennisSchedule
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
