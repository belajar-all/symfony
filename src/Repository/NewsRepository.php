<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }
    
    public function groupByPerson():array
    {
        $qb = $this->createQueryBuilder('n');
        
        $qb
            ->select('p.id, sum(n.id) as sum')
            ->join('n.person', 'p')
            ->groupBy('p.id')
                ->orderBy('p.id', 'asc')
            ;
    }
        
        
    public function latihanQuery()
    {
        $qb = $this->createQueryBuilder('t');
        $expr = $qb->expr();
        $qb->join('t.person', 'p');
        $qb
                ->where($expr->like('p.name', $expr->literal('%4%')));
                
//                ->where('t.title = :title AND t.person = :person')
//                    ->setParameter('title', 'tes judul berita 01')
//                    ->setParameter('person', $person)
//                ->addOrderBy('t.created_at', 'DESC')
//                ->setMaxResults(1);
        //dump($qb->getQuery()->getDQL());exit;
        return $qb->getQuery()->getResult();
    }
}
