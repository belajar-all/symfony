<?php

namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    public function getAllowedPersons():array
    {
        $qb = $this->createQueryBuilder('t')
                ->where('t.id > :id')->setParameter('id', 15)
                ->orderBy('t.name');
        return $qb->getQuery()->getResult();
    }
    
    public function getPersonArray():array
    {
        $data = [];
        foreach ($this->findAll() as $person) {
            $data[$person->getId()] = $person->getName();
        }
        
        return $data;
    }
}
