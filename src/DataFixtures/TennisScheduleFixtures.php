<?php

/**
 * This file is part of the symfony.
 */

namespace App\DataFixtures;

use App\Entity\TennisSchedule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Kematjaya\NaiveBayes\NaiveBayes;

/**
 * @package App\DataFixtures
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class TennisScheduleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $dataLatih = [
            [
                'K1' => 'SUNNY', 'K2' => 'HOT', 'K3' => "HIGH", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'NO' // 1
            ],
            [
                'K1' => 'SUNNY', 'K2' => 'HOT', 'K3' => "HIGH", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'NO' // 2
            ],
            [
                'K1' => 'OVERCAST', 'K2' => 'HOT', 'K3' => "HIGH", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' //3
            ],
            [
                'K1' => 'RAIN', 'K2' => 'MILD', 'K3' => "HIGH", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' // 4
            ],
            [
                'K1' => 'RAIN', 'K2' => 'COOL', 'K3' => "NORMAL", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' // 5
            ],
            [
                'K1' => 'RAIN', 'K2' => 'COOL', 'K3' => "NORMAL", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'NO' // 6
            ],
            [
                'K1' => 'OVERCAST', 'K2' => 'COOL', 'K3' => "NORMAL", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'YES' // 7
            ],
            [
                'K1' => 'SUNNY', 'K2' => 'MILD', 'K3' => "HIGH", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'NO' // 8
            ],
            [
                'K1' => 'SUNNY', 'K2' => 'COOL', 'K3' => "NORMAL", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' // 9
            ],
            [
                'K1' => 'RAIN', 'K2' => 'MILD', 'K3' => "NORMAL", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' // 10
            ],
            [
                'K1' => 'SUNNY', 'K2' => 'MILD', 'K3' => "NORMAL", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'YES' // 11
            ],
            [
                'K1' => 'OVERCAST', 'K2' => 'MILD', 'K3' => "HIGH", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'YES' // 12
            ],
            [
                'K1' => 'OVERCAST', 'K2' => 'HOT', 'K3' => "NORMAL", 'K4' => 'WEAK', NaiveBayes::TARGET_KEY => 'YES' // 13
            ],
            [
                'K1' => 'RAIN', 'K2' => 'MILD', 'K3' => "HIGH", 'K4' => 'STRONG', NaiveBayes::TARGET_KEY => 'NO' // 14
            ]
        ];
        
        foreach ($dataLatih as $value) {
            $tennisSchedule = new TennisSchedule();
            $tennisSchedule
                    ->setOutlook($value['K1'])
                    ->setTemperatur($value['K2'])
                    ->setHumidity($value['K3'])
                    ->setWind($value['K4'])
                    ->setOutput($value[NaiveBayes::TARGET_KEY]);
            
            $manager->persist($tennisSchedule);
        }
            
        $manager->flush();
    }
}
