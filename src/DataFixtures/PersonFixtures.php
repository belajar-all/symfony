<?php

/**
 * This file is part of the symfony.
 */

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @package App\DataFixtures
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PersonFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $person = new Person();
            $person->setName('Person ' . $i)
                    ->setAddress('test address');

            for ($x = 0; $x < rand(5, 10); $x++) {
                $news = new News();
                $news->setCreatedAt(new \DateTime())
                        ->setTitle('tes judul berita ' . $i . $x)
                        ->setContent('bgjhfhgfbghgfhgbfjghcbhbchgbfnjhgjgjhnj-' . $i . $x)
                        ->setPerson($person);
                
                $manager->persist($news);
            }
            
            $manager->persist($person);
        }
            
        $manager->flush();
    }
}
