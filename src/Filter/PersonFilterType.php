<?php

/**
 * Generated by kematjaya/crud-maker-bundle 
 * Report any bug on Girhub: https://github.com/kematjaya0/crud-maker-bundle 
 * form more information about Type, please visit https://github.com/lexik/LexikFormFilterBundle
 */
namespace App\Filter;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;

/**
 * Description of App\Filter\PersonFilterType *
 */
class PersonFilterType extends AbstractFilterType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', Filters\TextFilterType::class, [
           'condition_pattern' => FilterOperands::STRING_BOTH
        ])
                    ->add('address', Filters\TextFilterType::class, [
           'condition_pattern' => FilterOperands::STRING_BOTH
        ])
            ;
    }
}
