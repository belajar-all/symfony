<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Filter\NewsFilterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Kematjaya\BaseControllerBundle\Controller\BaseLexikFilterController as BaseController;

/**
 * @Route("/news", name="news_")
 */
class NewsController extends BaseController
{
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, NewsRepository $newsRepository): Response
    {
        $form = $this->createFormFilter(NewsFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $newsRepository->createQueryBuilder('this'));
                
        return $this->render('news/index.html.twig', [
            'news' => parent::createPaginator($queryBuilder, $request), 
            'filter' => $form->createView() 
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news, [
            'attr' => ['id' => 'ajaxForm', 'action' => $this->generateUrl('news_new')]
        ]);
        $result = parent::processFormAjax($request, $form);
        if ($result['process']) {
            return $this->json($result);
        }

        return $this->render('news/form.html.twig', [
            'news' => $news,
            'form' => $form->createView(), 'title' => 'new'
        ]);
    }

    /**
     * @Route("/{id}/show", name="show", methods={"GET"})
     */
    public function show(News $news): Response
    {
        return $this->render('news/show.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, News $news): Response
    {
        $form = $this->createForm(NewsType::class, $news, [
            'attr' => ['id' => 'ajaxForm', 'action' => $this->generateUrl('news_edit', ['id' => $news->getId()])]
        ]);
        
        $result = parent::processFormAjax($request, $form);
        if ($result['process']) {
            return $this->json($result);
        }

        return $this->render('news/form.html.twig', [
            'news' => $news,
            'form' => $form->createView(), 'title' => 'edit'
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, News $news): Response
    {
        $tokenName = 'delete'.$news->getId();
        parent::doDelete($request, $news, $tokenName);
        
        return $this->redirectToRoute('news_index');
    }
}
