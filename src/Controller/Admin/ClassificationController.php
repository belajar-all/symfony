<?php

/**
 * This file is part of the symfony.
 */

namespace App\Controller\Admin;

use App\Entity\TennisSchedule;
use App\Form\TennisScheduleType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\TennisScheduleRepository;
use Kematjaya\NaiveBayes\NaiveBayes;

/**
 * @Route("/classification", name="classification_")
 * @package App\Controller
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ClassificationController extends AbstractController
{
    
    /**
     * @Route("/", name="index")
     */
    public function index(TennisScheduleRepository $repo)
    {
        
        return $this->render('classification/index.html.twig', [
            'data' => $repo->findAll()
        ]);
    }
    
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function create(Request $request, TennisScheduleRepository $repo)
    {
        $form = $this->createForm(TennisScheduleType::class, new TennisSchedule(), [
            'attr' => ['id' => 'ajaxForms', 'action' => $this->generateUrl('classification_create')]
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $datas = $repo->findAll();
            
            $resultSet = [];
            foreach ($datas as $data) {
                $resultSet[] = [
                    'K1' => $data->getOutlook(),
                    'K2' => $data->getTemperatur(),
                    'K3' => $data->getHumidity(),
                    'K4' => $data->getWind(),
                    NaiveBayes::TARGET_KEY => $data->getOutput()
                ];
            }
            
            $uji = [
                'K1' => $form->getData()->getOutlook(),
                'K2' => $form->getData()->getTemperatur(),
                'K3' => $form->getData()->getHumidity(),
                'K4' => $form->getData()->getWind()
            ];
            
            $bayes = new NaiveBayes($resultSet);
            $result = $bayes->predict($uji);
            dump($uji);
            dump($result);
            exit;
        }
            
        return $this->render('classification/form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
