<?php

namespace App\Controller\Admin;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use App\Filter\PersonFilterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Kematjaya\Breadcrumb\Lib\Builder as BreacrumbBuilder;
use Kematjaya\BaseControllerBundle\Controller\BaseLexikFilterController as BaseController;

/**
 * @Route("/person", name="person_")
 */
class PersonController extends BaseController
{
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreacrumbBuilder $builder, PersonRepository $personRepository)//: Response
    {
        return ['test'];
        
        $builder->add('person');
        $form = $this->createFormFilter(PersonFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $personRepository->createQueryBuilder('this'));
                
        return $this->render('person/index.html.twig', [
            'people' => parent::createPaginator($queryBuilder, $request), 
            'filter' => $form->createView() 
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
        public function new(Request $request): Response
    {
            $person = new Person();
        
                $form = $this->createForm(PersonType::class, $person, [
            'attr' => ['id' => 'ajaxForm', 'action' => $this->generateUrl('person_new')]
        ]);
        $result = parent::process($request, $form);
        if ($result['process']) {
            return $this->json($result);
        }
                
        return $this->render('person/form.html.twig', [
            'person' => $person,
            'form' => $form->createView(), 'title' => 'new'
        ]);
    }

    /**
     * @Route("/{id}/show", name="show", methods={"GET"})
     */
        public function show( Person $person): Response
    {
            return $this->render('person/show.html.twig', [
            'person' => $person,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
        public function edit(Request $request, Person $person): Response
    {
                    $form = $this->createForm(PersonType::class, $person, [
            'attr' => ['id' => 'ajaxForm', 'action' => $this->generateUrl('person_edit', ['id' => $person->getId()])]
        ]);
        $result = parent::processFormAjax($request, $form);
        if ($result['process']) {
            return $this->json($result);
        }
         
        
        return $this->render('person/form.html.twig', [
            'person' => $person,
            'form' => $form->createView(), 'title' => 'edit'
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        $tokenName = 'delete'.$person->getId();
        parent::doDelete($request, $person, $tokenName);
        
        return $this->redirectToRoute('person_index');
    }
}
