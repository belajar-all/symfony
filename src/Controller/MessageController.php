<?php

/**
 * This file is part of the symfony.
 */

namespace App\Controller;

use App\Message\SmsNotification;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @package App\Controller
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class MessageController extends AbstractController 
{
    /**
     * @Route("/message", name="message")
     */
    public function index(MessageBusInterface $bus)
    {
        try {
            $bus->dispatch(new SmsNotification('test sms'), [
                new AmqpStamp('sms.test'), new DelayStamp(60000)
            ]);
        } catch (\Exception $ex) {
            dump($ex->getMessage());exit;
        }
        
        return new Response('test message');
    }
}
