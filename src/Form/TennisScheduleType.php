<?php

namespace App\Form;

use App\Entity\TennisSchedule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TennisScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('outlook', ChoiceType::class, [
                'choices' => array_combine(TennisSchedule::getOutlooks(), TennisSchedule::getOutlooks())
            ])
            ->add('temperatur', ChoiceType::class, [
                'choices' => array_combine(TennisSchedule::getTemperatures(), TennisSchedule::getTemperatures())
            ])
            ->add('humidity', ChoiceType::class, [
                'choices' => array_combine(TennisSchedule::getHumiditys(), TennisSchedule::getHumiditys())
            ])
            ->add('wind', ChoiceType::class, [
                'choices' => array_combine(TennisSchedule::getWinds(), TennisSchedule::getWinds())
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TennisSchedule::class,
        ]);
    }
}
