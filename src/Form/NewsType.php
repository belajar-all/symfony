<?php

namespace App\Form;

use App\Entity\News;
use App\Repository\PersonRepository;
use App\FormSubscriber\NewsTypeSubscriber;
use Symfony\Component\Form\AbstractType;
use Kematjaya\HiddenTypeBundle\Type\HiddenDateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class NewsType extends AbstractType
{
    private $repo;
    
    public function __construct(PersonRepository $repo) 
    {
        $this->repo = $repo;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at', HiddenDateTimeType::class)
            ->add('title')
            ->add('content', ChoiceType::class, [
                'choices' => array_flip($this->repo->getPersonArray())
            ])
            ->add('person', EntityType::class, [
                'class' => \App\Entity\Person::class
            ])
        ;
        
        //// CARA 1
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event){
            $event->getForm()
                    ->add('title', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
                        'attr' => ['onchange' => 'alert("aa")']
                    ]);
        });
        
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (strlen($data->getTitle()) > 4) {
                $event
                        ->getForm()
                        ->get('title')
                        ->addError(new FormError("judul tidak boleh lebih dari 4 karakter"));
            }
        });
        
        // CARA 2
        $builder->addEventSubscriber(new NewsTypeSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
