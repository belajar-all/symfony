<?php

namespace App\Entity;

use App\Repository\TennisScheduleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TennisScheduleRepository::class)
 */
class TennisSchedule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $outlook;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $temperatur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $humidity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wind;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $output;

    const OUTLOOK_SUNNY = 'SUNNY';
    const OUTLOOK_OVERCAST = 'OVERCAST';
    const OUTLOOK_RAIN = 'RAIN';
    
    const TEMP_HOT = 'HOT';
    const TEMP_MILD = 'MILD';
    const TEMP_COOL = 'COOL';
    
    const HUMIDITY_HIGH = 'HIGH';
    const HUMIDITY_NORMAL = 'NORMAL';
    
    const WIND_WEAK = 'WEAK';
    const WIND_STRONG = 'STRONG';
    
    public static function getOutlooks():array
    {
        return [
            self::OUTLOOK_SUNNY, self::OUTLOOK_OVERCAST, self::OUTLOOK_RAIN
        ];
    }
    
    public static function getTemperatures():array
    {
        return [
            self::TEMP_COOL, self::TEMP_MILD, self::TEMP_HOT
        ];
    }
    
    public static function getHumiditys():array
    {
        return [
            self::HUMIDITY_NORMAL, self::HUMIDITY_HIGH
        ];
    }
    
    public static function getWinds():array
    {
        return [
            self::WIND_WEAK, self::WIND_STRONG
        ];
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOutlook(): ?string
    {
        return $this->outlook;
    }

    public function setOutlook(string $outlook): self
    {
        $this->outlook = $outlook;

        return $this;
    }

    public function getTemperatur(): ?string
    {
        return $this->temperatur;
    }

    public function setTemperatur(string $temperatur): self
    {
        $this->temperatur = $temperatur;

        return $this;
    }

    public function getHumidity(): ?string
    {
        return $this->humidity;
    }

    public function setHumidity(string $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getWind(): ?string
    {
        return $this->wind;
    }

    public function setWind(string $wind): self
    {
        $this->wind = $wind;

        return $this;
    }

    public function getOutput(): ?string
    {
        return $this->output;
    }

    public function setOutput(string $output): self
    {
        $this->output = $output;

        return $this;
    }
}
