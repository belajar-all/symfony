<?php

/**
 * This file is part of the symfony.
 */

namespace App\MessageHandler;

use App\Message\SmsNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @package App\MessageHandler
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SmsNotificationHandler implements MessageHandlerInterface 
{
    public function __invoke(SmsNotification $message)
    {
        dump($message->getContent());
    }
}
