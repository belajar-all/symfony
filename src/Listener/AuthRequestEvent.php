<?php

/**
 * This file is part of the symfony.
 */

namespace App\Listener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * @package App\Listener
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class AuthRequestEvent 
{
    /**
     * 
     * @param RequestEvent $event
     * @return type
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if(!$event->isMasterRequest()) {
            
            return;
        }
        
        echo 'kernel_request <br/>';
        return;
    }
    
    /**
     * 
     * @param ControllerEvent $event
     * @return type
     */
    public function onKernelController(ControllerEvent $event)
    {
        echo 'kernel_controller';
        
        return;
    }
    
    /**
     * dijalankan hanya ketika output dari controller bukan object Response 
     * @param ViewEvent $event
     */
    public function onKernelView(ViewEvent $event)
    {
        $data = [
            'status' => 200,
            'data' => $event->getControllerResult()
        ];
        
        $response = new \Symfony\Component\HttpFoundation\JsonResponse($data);
        $event->setResponse($response);
    }
    
    /**
     * dijalankan jika output dr controller berupa object dari Response / setelah onKernelView
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        dump($event);exit;
    }
}
