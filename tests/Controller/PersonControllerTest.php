<?php

namespace App\Tests\Controller;

use App\Entity\Person; 
use Kematjaya\BaseControllerBundle\FunctionalTest\Controller\AbstractCRUDControllerTest;

class PersonControllerTest extends AbstractCRUDControllerTest 
{
    protected function buildObject() 
    {
        $person = $this->doctrine->getRepository(Person::class)->createQueryBuilder('t')
                ->setMaxResults(1)->getQuery()->getOneOrNullResult();
        if (!$person) {
            $person = new Person();
            $person->setName('test')->setAddress('add'); 
        }
        
        return $person;
    }
    
    public function testIndex()
    {
        parent::doIndex($this->router->generate('person_index'), true);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'name' => $data->getName(),
            'address' => $data->getAddress() 
        ];
        
        $url = $this->generate('person_new');
        parent::processForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'name' => $data->getName(),
            'address' => $data->getAddress() 
        ];
        
        $url = $this->generate('person_edit', ["id" => $data->getId()]);
        parent::processForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        
        $url = $this->generate('person_show', ["id" => $data->getId()]);
        parent::doShow($url, $data);
    }
    
    public function testDelete()
    {
        $data = $this->buildObject();
        $url = $this->generate('person_delete', ["id" => $data->getId()]);
        parent::doDelete($url, $data);
    }
}
